package com.twuc.webApp;

        import org.springframework.web.bind.annotation.*;

        import java.time.LocalDate;
        import java.time.format.DateTimeFormatter;
        import java.util.Date;
        import java.util.List;
        import java.util.Locale;

@RestController
public class UserController {
    @GetMapping("/api/users/{userId}")
    public String bindToInteger(@PathVariable Integer userId) {
        return "bind to integer success";
    }
    @GetMapping("/api/users/bindint/{userId}")
    public String bindToInteger(@PathVariable int userId) {
        return "bind to int success";
    }
    @GetMapping("/api/users/{userId}/books/{bookId}")
    public String bindToMultipleParameter(@PathVariable("userId") Integer userid, @PathVariable Integer bookId) {
        return "bind to multiple parameter success";
    }
    @GetMapping("/api/users")
    public String bindToMultipleParameter(@RequestParam Integer bookid) {
        return "bind to query parameter";
    }
    @GetMapping("/api/users/books")
    public String bindTodefaultParameter(@RequestParam(defaultValue = "x112") String bookid) {
        return "bind to default parameter";
    }
    @GetMapping("/api/users/booksItem")
    public String bindToCollectionParameter(@RequestParam List<String> booklist) {
        return "bind to Collection parameter";
    }
    @PostMapping("api/datetimes")
    public String datetimesTransferToObejct(@RequestParam("date") LocalDate date) {
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        date = LocalDate.parse("2019-10-01T10:00:00Z", inputFormatter);
        String formattedDate = inputFormatter.format(date);
        return formattedDate;
    }

}
