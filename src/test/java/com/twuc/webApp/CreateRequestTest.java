package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateRequestTest {
    @Test
    public void objectToJson() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        Person person = new Person("xiaoming", "male");
        String actual = objectMapper.writeValueAsString(person);
        assertEquals("{\"name\":\"xiaoming\",\"gender\":\"male\"}",actual);
    }
    @Test
    public void jsonToObject() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Person person = objectMapper.readValue("{\"name\":\"xiaoming\",\"gender\":\"male\"}", Person.class);
        assertEquals("xiaoming", person.getName());
    }


}
