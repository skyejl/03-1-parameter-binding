package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Test
    public void should_bind_to_Integer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("bind to integer success"));
    }
    @Test
    public void should_bind_to_int() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/bindint/3"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("bind to int success"));
    }
    @Test
    public void should_bind_to_multiple_parameter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/3/books/4"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("bind to multiple parameter success"));
    }
    @Test
    public void should_bind_to_query_parameter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users?bookid=3"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("bind to query parameter"));
    }
    @Test
    public void should_bind_to_default_parameter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("bind to default parameter"));
    }
    @Test
    public void should_bind_to_collection_parameter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/booksItem?booklist=1,2,3"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("bind to Collection parameter"));
    }
}
